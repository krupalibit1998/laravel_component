@extends('test::app')
@section('content')
<x-courier-header/>
<h4><a href="{{ route('task_list') }}">Back </a></h4>
<form action="{{ route('store') }}" method="post">
    {{ csrf_field() }}          
    <div class="form-inline">
    	<h4>Add To Do Tasks: </h4>
        <div class="form-group">
        	<label>Name</label>
             <input type="text" name="name" class="form-control" required />
        </div>
        <div class="form-group">
             <input type="submit" class="btn btn-primary form-control" />
        </div>
    </div>
</form>
<x-courier-footer/>
 @endsection