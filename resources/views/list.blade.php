@extends('test::app')
@section('content')
<x-courier-header/>
<h4><a href="{{ route('add_data') }}">Add Task</a></h4>
<h4>Tasks To Do : </h4>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($tasks as $task)
                <tr>
                    <td>{{ $task->name }}</td>
                    <td>
                        <div class='btn-group'>
                            <a href="{{ route('edit_data',$task->id) }}"  class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                
                        </div>
                        
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
<x-courier-footer/>
@endsection