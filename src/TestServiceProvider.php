<?php 

namespace ToDo;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use ToDo\View\Components\Header;
use ToDo\View\Components\Footer;


class TestServiceProvider extends ServiceProvider
{
	public function boot()
	{
		$this->loadMigrationsFrom(__DIR__.'/./../publishable/database/migrations');
        $this->loadViewsFrom(__DIR__.'/./../resources/views','test');
        $this->publishes([
        __DIR__.'/./../resources/views' => resource_path('views/vendor/test'),
    	]);
        
        $this->app['router']->namespace('ToDo\Http\Controllers')
                ->middleware(['web'])
                ->group(function () {
                    $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
                });
        $this->loadViewComponentsAs('courier', [
        Header::class,
        Footer::class
    ]);


	}
	public function register()
	{
		//$this->registerpublishable();
	}

	public function registerpublishable()
	{
		$basepath = dirname(__DIR__);

		$arrpublishable = [
			"migrations"=>[
				"$basepath/publishable/database/migrations"=>databse_path('migrations'),
			]];
	}
}