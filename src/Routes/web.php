<?php 

// $namespace = 'ToDo\Http\Controllers';
// Route::group(['namespace'=>$namespace,'prefix'=>'test',],function(){
// });

Route::get('/task_list', 'TaskController@index')->name('task_list');
Route::get('/add_data', 'TaskController@create')->name('add_data');
Route::get('/edit_data/{id}', 'TaskController@edit')->name('edit_data');
Route::post('/update_data/{id}', 'TaskController@update')->name('update_data');
Route::post('/store', 'TaskController@store')->name('store');