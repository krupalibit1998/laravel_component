<?php

namespace ToDo\Http\Controllers;

use App\Http\Controllers\Controller;
use Request;
use ToDo\Models\Task;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        return view('test::list',compact('tasks'));
    }

    public function create()
    {
        $tasks = Task::all();
        $submit = 'Add';
        return view('test::add', compact('tasks', 'submit'));
    }

    public function store()
    {
        $input = Request::all();
        Task::create($input);
        return redirect()->route('add_data');
    }

    public function edit($id)
    {
        $tasks = Task::all();
        $task = $tasks->find($id);
        $submit = 'Update';
        return view('test::edit', compact('task', 'submit'));
    }

    public function update($id)
    {
        $input = Request::all();
        $task = Task::findOrFail($id);
        $task->update($input);
        return redirect()->route('task_list');
    }

    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
        return redirect()->route('add_data');
    }
}